# Find pair
___
## How the Find pair game works
The game board consists of sixteen "cards" arranged in a grid. The deck is made up of eight different pairs of cards, each with different color on one side. The cards are arranged randomly on the grid with the color face down. The gameplay rules are very simple: flip over two hidden cards at a time to locate the ones that match!
 
## How to play the game
To play the game you need to upload the following files to your computer's hard drive:

- _index.html_
- _js_ folder
- _css_ folder
- _img_ folder

**OR**

```git clone https://denisdmitriyeff@bitbucket.org/denisdmitriyeff/find-pair.git```
___
When you have uploaded the needed files open **index.html** through your web browser.
